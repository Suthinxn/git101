def bubble_sort(numbers):
    # pass # write your code hear
    number = len(numbers)
    for i in range(number-1):
        for j in range(number-1):
            if (numbers[j] > numbers[j + 1]):
                temp = numbers[j]
                numbers[j] = numbers[j+1]
                numbers[j + 1] = temp
    return numbers

if __name__ == "__main__":
    numbers = list(map(int, input("Enter integer number with space: ")))
    sorted_numbers = bubble_sort(numbers)
print("Sorted number is", sorted_numbers)

"""
Ref : https://sunny420x.com/articles/47
"""