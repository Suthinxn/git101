def selection_sort(numbers):
    number = len(numbers)
    for i in range(number - 1):
        for j in range(i + 1, number):
            if (numbers[j] < numbers[i]):
                temp = numbers[j]
                numbers[j] = numbers[i]
                numbers[i] = temp
                print("i =",i, "j =",j,numbers)


if __name__ == "__main__":
    numbers = list(map(int, input("Enter integer number with space: ")))
    sorted_numbers = selection_sort(numbers)
print("Sorted number is", sorted_numbers)

"""
Ref : https://sunny420x.com/articles/51
"""